package org.umss.gea.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.umss.gea.domain.JobTitle;

public interface JobTitleRepository extends CrudRepository<JobTitle, Integer> {
    @Query("SELECT f FROM JobTitle f WHERE f.uuid = ?1")
    JobTitle findOneByUuid(String uuid);
}

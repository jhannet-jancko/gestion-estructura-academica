package org.umss.gea.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.umss.gea.domain.University;

public interface UniversityRepository extends JpaRepository<University, Integer> {

    @Query("SELECT u FROM University u WHERE u.uuid = ?1")
    University findOneByUuid(String uuid);
}
package org.umss.gea.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.umss.gea.domain.Authority;

import java.util.List;

public interface AuthorityRepository extends JpaRepository<Authority, Integer> {
    @Query("SELECT a FROM Authority a WHERE a.faculty.id = ?1 AND a.active = true")
    List<Authority> findAllByFacultyId(Integer id);
}

package org.umss.gea.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.umss.gea.domain.Professor;

public interface ProfessorRepository extends JpaRepository<Professor, Integer> {
    @Query("SELECT f FROM Professor f WHERE f.uuid = ?1")
    Professor findOneByUuid(String uuid);
}

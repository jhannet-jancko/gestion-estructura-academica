package org.umss.gea.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class AuthorityDTO {
    private  String uuid;
    private String facultyUuid;
    private String professorUuid;
    private String jobTitleUuid;
    private Date startDate;
    private Date endDate;
    private Date createdDate;
    private Date modifiedDate;
    private Boolean active = Boolean.TRUE;
}

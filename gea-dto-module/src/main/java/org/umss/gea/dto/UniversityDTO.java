package org.umss.gea.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class UniversityDTO {
    private String uuid;
    @NotBlank
    @Size(max = 500, min = 2)
    private String name;
    @NotBlank
    @Size(max = 20, min = 2, message = "Code should has more than 2 letters")
    private String code;
    private Date createdDate;
    private Date modifiedDate;

    public UniversityDTO(String uuid, String code, String name) {
        this.uuid = uuid;
        this.code = code;
        this.name = name;
    }
}

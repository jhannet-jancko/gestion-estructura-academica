package org.umss.gea.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class ProfessorDTO {
    private String uuid;
    @NotBlank
    @Size(max = 20, min = 2, message = "Code should has more than 2 letters")
    private String code;
    @NotBlank
    @Size(max = 100, min = 2)
    private String firstName;
    @NotBlank
    @Size(max = 100, min = 2)
    private String lastName;
    private String professionalTitle;

    private Date createdDate;
    private Date modifiedDate;
    private Boolean active = Boolean.TRUE;
}

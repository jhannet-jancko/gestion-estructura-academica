package org.umss.gea.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class JobTitleDTO {
    private String uuid;
    @NotBlank
    private String nameTitle;
    private Date createdDate;
    private Date modifiedDate;
    private Boolean active = Boolean.TRUE;
}

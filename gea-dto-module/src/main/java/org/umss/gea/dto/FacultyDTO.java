package org.umss.gea.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class FacultyDTO {
    private String uuid;
    @NotBlank
    @Size(max = 500, min = 2)
    private String name;
    @NotBlank
    @Size(max = 20, min = 2, message = "Code should has more than 2 letters")
    private String code;
    private String address;
    private String mission;
    private String vision;
    private String telephone;
    private String email;
    private String webSite;
    private String managementType;
    private Date createdDate;
    private Date modifiedDate;
    private Boolean active = Boolean.TRUE;

    private UniversityDTO university;
}

package org.umss.gea.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

import static org.umss.gea.domain.utils.constants.DBConstants.FACULTY_TABLE_NAME_DB;

@Entity
@Table(name = FACULTY_TABLE_NAME_DB)
@Data
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@SQLDelete(sql = "UPDATE faculty SET active = false WHERE id=?")
public class Faculty {
    @Id
    @SequenceGenerator(name = "faculty_id_seq")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(updatable = false, nullable = false, unique = true, length = 36)
    private String uuid;
    @Column(nullable = false, length = 500)
    private String name;
    @Column(nullable = false, length = 20)
    private String code;
    @Column(length = 500)
    private String address;
    @Column(length = 1000)
    private String mission;
    @Column(length = 1000)
    private String vision;
    @Column(length = 20)
    private String telephone;
    @Column(length = 350)
    private String email;
    @Column(length = 1000)
    private String webSite;
    @Column(name = "management_type")
    private String managementType;
    @ManyToOne(fetch = FetchType.LAZY)
    private University university;
    @CreatedDate
    @Column(name = "created_date", updatable = false, columnDefinition = "timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP")
    private Date createdDate;
    @LastModifiedDate
    @Column(name = "modified_date", columnDefinition = "timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP")
    private Date modifiedDate;
    @Column(columnDefinition = "BOOLEAN NOT NULL DEFAULT '1'")
    private Boolean active;

    public Faculty(String uuid) {
        this.uuid = uuid;
    }

    @PrePersist
    public void initializeUuid() {
        this.setUuid(UUID.randomUUID().toString());
    }

    @Override
    public String toString() {
        return "Faculty{" +
                "id=" + id +
                ", university=" + university +
                ", uuid='" + uuid + '\'' +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", creationDate=" + createdDate +
                ", modifiedDate=" + modifiedDate +
                '}';
    }
}

package org.umss.gea.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

import static org.umss.gea.domain.utils.constants.DBConstants.UNIVERSITY_TABLE_NAME_DB;

@Entity
@Table(name = UNIVERSITY_TABLE_NAME_DB)
@Data
@NoArgsConstructor
public class University {

    @Id
    @GeneratedValue
    private Integer id;
    @Column(updatable = false, nullable = false, unique = true, length = 36)
    private String uuid;
    @Column(nullable = false, length = 500)
    private String name;
    @Column(nullable = false, length = 20)
    private String code;
    @CreatedDate
    @Column(updatable = false, columnDefinition = "timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP")
    private Date createdDate;
    @LastModifiedDate
    @Column(columnDefinition = "timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP")
    private Date modifiedDate;
    @OneToMany(mappedBy = "university", cascade = CascadeType.REMOVE)
    private List<Faculty> facultyList;
    @Column(columnDefinition = "BOOLEAN NOT NULL DEFAULT '1'")
    public Boolean active;

    public University(String uuid) {
        this.uuid = uuid;
    }
}

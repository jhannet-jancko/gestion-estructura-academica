package org.umss.gea.domain.utils.constants;

public interface DBConstants {
    String UNIVERSITY_TABLE_NAME_DB = "university";
    String FACULTY_TABLE_NAME_DB = "faculty";
    String PROFESSOR_TABLE_NAME_DB = "professor";
    String JOB_TITLE_TABLE_NAME_DB = "job_title";
    String AUTHORITY_TABLE_NAME_DB = "faculty_professor_title";
}

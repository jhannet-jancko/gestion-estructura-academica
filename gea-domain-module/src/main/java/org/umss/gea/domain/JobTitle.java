package org.umss.gea.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

import static org.umss.gea.domain.utils.constants.DBConstants.JOB_TITLE_TABLE_NAME_DB;

@Entity
@Table(name = JOB_TITLE_TABLE_NAME_DB)
@Data
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@SQLDelete(sql = "UPDATE jobTitle SET active = false WHERE id=?")
public class JobTitle {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(updatable = false, nullable = false, unique = true, length = 36)
    private String uuid;
    @Column(nullable = false, length = 300)
    private String nameTitle;

    @CreatedDate
    @Column(name = "created_date", updatable = false, columnDefinition = "timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP")
    private Date createdDate;
    @LastModifiedDate
    @Column(name = "modified_date", columnDefinition = "timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP")
    private Date modifiedDate;
    @Column(columnDefinition = "BOOLEAN NOT NULL DEFAULT '1'")
    private Boolean active;

    public JobTitle(String uuid) {
        this.uuid = uuid;
    }
}

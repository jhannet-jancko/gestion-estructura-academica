package org.umss.gea.service.mapper;

import org.springframework.stereotype.Component;
import org.umss.gea.domain.Professor;
import org.umss.gea.dto.ProfessorDTO;
import org.umss.gea.service.CustomMapper;

@Component
public class ProfessorMapper implements CustomMapper<ProfessorDTO, Professor> {
    @Override
    public ProfessorDTO toDto(Professor professor) {
        ProfessorDTO professorDTO = new ProfessorDTO();
        professorDTO.setUuid(professor.getUuid());
        professorDTO.setCode(professor.getCode());
        professorDTO.setFirstName(professor.getFirstName());
        professorDTO.setLastName(professor.getLastName());
        professorDTO.setProfessionalTitle(professor.getProfessionalTitle());
        professorDTO.setCreatedDate(professor.getCreatedDate());
        professorDTO.setModifiedDate(professor.getModifiedDate());
        professorDTO.setActive(professor.getActive());

        return professorDTO;
    }

    @Override
    public Professor toEntity(ProfessorDTO professorDTO) {
        Professor professor = new Professor();
        professor.setUuid(professorDTO.getUuid());
        professor.setCode(professorDTO.getCode());
        professor.setFirstName(professorDTO.getFirstName());
        professor.setLastName(professorDTO.getLastName());
        professor.setProfessionalTitle(professorDTO.getProfessionalTitle());
        professor.setCreatedDate(professorDTO.getCreatedDate());
        professor.setModifiedDate(professorDTO.getModifiedDate());
        professor.setActive(professorDTO.getActive());

        return professor;
    }
}

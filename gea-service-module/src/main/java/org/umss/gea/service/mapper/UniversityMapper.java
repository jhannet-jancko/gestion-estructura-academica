package org.umss.gea.service.mapper;

import org.springframework.stereotype.Component;
import org.umss.gea.domain.University;
import org.umss.gea.dto.UniversityDTO;
import org.umss.gea.service.CustomMapper;

@Component
public class UniversityMapper implements CustomMapper<UniversityDTO, University> {
    @Override
    public UniversityDTO toDto(University university) {
        UniversityDTO universityDTO = new UniversityDTO();
        universityDTO.setUuid(university.getUuid());
        universityDTO.setCode(university.getCode());
        universityDTO.setName(university.getName());
        universityDTO.setCreatedDate(university.getCreatedDate());
        universityDTO.setModifiedDate(university.getModifiedDate());

        return universityDTO;
    }

    @Override
    public University toEntity(UniversityDTO universityDTO) {
        University university = new University();
        university.setUuid(universityDTO.getUuid());
        university.setCode(universityDTO.getCode());
        university.setName(universityDTO.getName());
        university.setCreatedDate(universityDTO.getCreatedDate());
        university.setModifiedDate(universityDTO.getModifiedDate());

        return university;
    }
}

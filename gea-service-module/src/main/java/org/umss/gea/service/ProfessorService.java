package org.umss.gea.service;

import org.umss.gea.dto.ProfessorDTO;

public interface ProfessorService extends CrudServiceBase<ProfessorDTO, String>{
}

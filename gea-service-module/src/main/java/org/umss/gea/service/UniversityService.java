package org.umss.gea.service;

import org.umss.gea.dto.FacultyDTO;
import org.umss.gea.dto.UniversityDTO;

import java.util.List;

public interface UniversityService extends CrudServiceBase<UniversityDTO, String>{
    List<FacultyDTO> getFaculties(String universityUuid);
}

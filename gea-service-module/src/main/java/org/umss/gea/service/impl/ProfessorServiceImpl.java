package org.umss.gea.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.umss.gea.common.exceptions.ResourceNotFoundException;
import org.umss.gea.domain.Professor;
import org.umss.gea.dto.ProfessorDTO;
import org.umss.gea.repository.ProfessorRepository;
import org.umss.gea.service.ProfessorService;
import org.umss.gea.service.mapper.ProfessorMapper;

import java.util.List;

import static org.umss.gea.domain.utils.constants.DBConstants.PROFESSOR_TABLE_NAME_DB;

@Service
public class ProfessorServiceImpl implements ProfessorService {

    private final ProfessorMapper professorMapper;
    private final ProfessorRepository professorRepository;

    public ProfessorServiceImpl(ProfessorMapper professorMapper, ProfessorRepository professorRepository) {
        this.professorMapper = professorMapper;
        this.professorRepository = professorRepository;
    }

    @Override
    public ProfessorDTO save(ProfessorDTO professorDTO) {
        Professor professor = professorMapper.toEntity(professorDTO);
        return professorMapper.toDto(professorRepository.save(professor));
    }

    @Override
    @Transactional(readOnly = true)
    public ProfessorDTO findByUuid(String uuid) {
        Professor professor = professorRepository.findOneByUuid(uuid);
        if (professor == null) {
            throw new ResourceNotFoundException(
                    String.format("Professor with uuid: %s does not exist", uuid),
                    PROFESSOR_TABLE_NAME_DB);
        }
        return professorMapper.toDto(professor);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProfessorDTO> findAll() {
        return professorRepository.findAll().stream().map(professorMapper::toDto).toList();
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProfessorDTO> findAllActive() {
        List<Professor> activeProfessors = professorRepository.findAll().stream()
                .filter(Professor::getActive)
                .toList();
        return activeProfessors.stream().map(professorMapper::toDto).toList();
    }

    @Override
    public ProfessorDTO logicalDelete(String uuid) {
        return null;
    }
}

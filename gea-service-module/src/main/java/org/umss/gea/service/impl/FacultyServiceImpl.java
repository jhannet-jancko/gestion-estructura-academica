package org.umss.gea.service.impl;

import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.umss.gea.common.exceptions.BadRequestAlertException;
import org.umss.gea.common.exceptions.ResourceNotFoundException;
import org.umss.gea.domain.Authority;
import org.umss.gea.domain.Faculty;
import org.umss.gea.domain.University;
import org.umss.gea.dto.AuthorityDTO;
import org.umss.gea.dto.FacultyDTO;
import org.umss.gea.dto.UniversityDTO;
import org.umss.gea.repository.AuthorityRepository;
import org.umss.gea.repository.FacultyRepository;
import org.umss.gea.repository.UniversityRepository;
import org.umss.gea.service.FacultyService;
import org.umss.gea.service.mapper.AuthorityMapper;
import org.umss.gea.service.mapper.FacultyMapper;

import java.util.List;
import java.util.Optional;

import static org.umss.gea.common.exceptions.ErrorConstants.VALUE_NULL_BAD_REQUEST;
import static org.umss.gea.domain.utils.constants.DBConstants.FACULTY_TABLE_NAME_DB;

@Service
@Transactional
public class FacultyServiceImpl implements FacultyService {

    private final FacultyRepository facultyRepository;
    private final FacultyMapper facultyMapper;
    private final UniversityRepository universityRepository;
    private final AuthorityRepository authorityRepository;
    private final AuthorityMapper authorityMapper;

    public FacultyServiceImpl(
            FacultyRepository facultyRepository,
            FacultyMapper facultyMapper,
            UniversityRepository universityRepository,
            AuthorityRepository authorityRepository, AuthorityMapper authorityMapper) {
        this.facultyRepository = facultyRepository;
        this.facultyMapper = facultyMapper;
        this.universityRepository = universityRepository;
        this.authorityRepository = authorityRepository;
        this.authorityMapper = authorityMapper;
    }

    @Override
    public FacultyDTO save(FacultyDTO facultyDTO) {
        UniversityDTO universityDTO = facultyDTO.getUniversity();
        if (universityDTO == null) {
            throw new BadRequestAlertException(
                    "A new faculty cannot have university as null value",
                    FACULTY_TABLE_NAME_DB, VALUE_NULL_BAD_REQUEST);
        }

        University university = universityRepository.findOneByUuid(universityDTO.getUuid());

        if (university == null) {
            throw new ResourceNotFoundException(
                    String.format("University with uuid: %s does not exist", universityDTO.getUuid()),
                    FACULTY_TABLE_NAME_DB);
        }

        Faculty faculty = facultyMapper.toEntity(facultyDTO, university);
        facultyRepository.save(faculty);

        return facultyMapper.toDto(faculty, true);
    }


    public FacultyDTO updateFaculty(FacultyDTO facultyDTO) {
        Faculty facultyExample = new Faculty(facultyDTO.getUuid());
        Optional<Faculty> optionalFaculty = facultyRepository.findOne(Example.of(facultyExample));

        if (optionalFaculty.isEmpty()) {
            throw new ResourceNotFoundException(
                    String.format("Faculty with uuid: %s does not exist", facultyDTO.getUuid()),
                    FACULTY_TABLE_NAME_DB);
        }

        UniversityDTO universityDTO = facultyDTO.getUniversity();
        University university = universityRepository.findOneByUuid(universityDTO.getUuid());

        if (university == null) {
            throw new ResourceNotFoundException(
                    String.format("University with uuid: %s does not exist", universityDTO.getUuid()),
                    FACULTY_TABLE_NAME_DB);
        }

        Faculty faculty = optionalFaculty.get();
        Faculty facultyToUpdate = facultyMapper.toEntity(facultyDTO, university);
        facultyToUpdate.setId(faculty.getId());
        facultyToUpdate.setCreatedDate(faculty.getCreatedDate());
        facultyToUpdate.setModifiedDate(faculty.getModifiedDate());

        return facultyMapper.toDto(facultyRepository.save(facultyToUpdate), true);
    }

    @Override
    @Transactional(readOnly = true)
    public FacultyDTO findByUuid(String uuid) {
        Faculty facultyExample = new Faculty(uuid);
        Optional<Faculty> optionalFaculty = facultyRepository.findOne(Example.of(facultyExample));
        if (optionalFaculty.isEmpty()) {
            throw new ResourceNotFoundException(
                    String.format("Faculty with uuid: %s does not exist", uuid),
                    FACULTY_TABLE_NAME_DB);
        }
        return facultyMapper.toDto(optionalFaculty.get(), true);
    }

    @Override
    @Transactional(readOnly = true)
    public List<FacultyDTO> findAll() {
        return facultyRepository.findAll().stream().map(faculty -> facultyMapper.toDto(faculty, true)).toList();
    }

    @Override
    @Transactional(readOnly = true)
    public List<FacultyDTO> findAllActive() {
        List<Faculty> activeFaculties = facultyRepository.findAll().stream()
                .filter(Faculty::getActive)
                .toList();
        return activeFaculties.stream().map(faculty -> facultyMapper.toDto(faculty, true)).toList();
    }

    @Override
    public FacultyDTO logicalDelete(String uuid) {
        Faculty facultyExample = new Faculty(uuid);
        Optional<Faculty> optionalFaculty = facultyRepository.findOne(Example.of(facultyExample));

        if (optionalFaculty.isEmpty()) {
            throw new ResourceNotFoundException(
                    String.format("Faculty with uuid: %s does not exist", uuid),
                    FACULTY_TABLE_NAME_DB);
        }

        Faculty faculty = optionalFaculty.get();
        facultyRepository.delete(faculty);

        return facultyMapper.toDto(faculty, true);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AuthorityDTO> getAuthorities(String facultyUuid) {
        Faculty faculty = facultyRepository.findOneByUuid(facultyUuid);

        if (faculty == null) {
            throw new ResourceNotFoundException(
                    String.format("Faculty with uuid: %s does not exist", facultyUuid),
                    FACULTY_TABLE_NAME_DB);
        }

        Faculty facultyExample = new Faculty();
        facultyExample.setId(faculty.getId());

        Authority authorityExample = new Authority();
        authorityExample.setFaculty(facultyExample);
        authorityExample.setActive(true);

        List<Authority> authorities = authorityRepository.findAllByFacultyId(faculty.getId());
        return authorities
                .stream()
                .map(authorityMapper::toDto)
                .toList();
    }
}

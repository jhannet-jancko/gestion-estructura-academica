package org.umss.gea.service;

import org.umss.gea.dto.JobTitleDTO;

public interface JobTitleService extends CrudServiceBase<JobTitleDTO, String>{
}

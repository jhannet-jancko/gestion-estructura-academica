package org.umss.gea.service.impl;

import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.umss.gea.common.exceptions.BadRequestAlertException;
import org.umss.gea.common.exceptions.ResourceNotFoundException;
import org.umss.gea.domain.*;
import org.umss.gea.dto.AuthorityDTO;
import org.umss.gea.repository.AuthorityRepository;
import org.umss.gea.repository.FacultyRepository;
import org.umss.gea.repository.JobTitleRepository;
import org.umss.gea.repository.ProfessorRepository;
import org.umss.gea.service.AuthorityService;

import java.util.List;
import java.util.Optional;

import static org.umss.gea.common.exceptions.ErrorConstants.ID_EXISTS_BAD_REQUEST;
import static org.umss.gea.domain.utils.constants.DBConstants.AUTHORITY_TABLE_NAME_DB;

@Service
public class AuthorityServiceImpl implements AuthorityService {

    private final AuthorityRepository authorityRepository;
    private final FacultyRepository facultyRepository;
    private final ProfessorRepository professorRepository;
    private final JobTitleRepository jobTitleRepository;

    public AuthorityServiceImpl(AuthorityRepository authorityRepository,
                                FacultyRepository facultyRepository,
                                ProfessorRepository professorRepository,
                                JobTitleRepository jobTitleRepository) {
        this.authorityRepository = authorityRepository;
        this.facultyRepository = facultyRepository;
        this.professorRepository = professorRepository;
        this.jobTitleRepository = jobTitleRepository;
    }

    @Override
    public AuthorityDTO save(AuthorityDTO authorityDTO) {
        Faculty faculty = this.facultyRepository.findOneByUuid(authorityDTO.getFacultyUuid());
        Professor professor = this.professorRepository.findOneByUuid(authorityDTO.getProfessorUuid());
        JobTitle jobTitle = this.jobTitleRepository.findOneByUuid(authorityDTO.getJobTitleUuid());

        if(faculty == null) {
            throw new ResourceNotFoundException(
                    String.format("Faculty with uuid: %s does not exist", authorityDTO.getUuid()),
                    AUTHORITY_TABLE_NAME_DB);
        }
        if(professor == null) {
            throw new ResourceNotFoundException(
                    String.format("Professor with uuid: %s does not exist", authorityDTO.getUuid()),
                    AUTHORITY_TABLE_NAME_DB);
        }
        if(jobTitle == null) {
            throw new ResourceNotFoundException(
                    String.format("Job Title with uuid: %s does not exist", authorityDTO.getUuid()),
                    AUTHORITY_TABLE_NAME_DB);
        }

        Authority authorityExample = new Authority();
        authorityExample.setFaculty(new Faculty(faculty.getUuid()));
        authorityExample.setProfessor(new Professor(professor.getUuid()));
        authorityExample.setJobTitle(new JobTitle(jobTitle.getUuid()));
        Optional<Authority> optionalAuthority = authorityRepository.findOne(Example.of(authorityExample));

        if (!optionalAuthority.isEmpty()) {
            throw new BadRequestAlertException("Authority already exist",
                    AUTHORITY_TABLE_NAME_DB, ID_EXISTS_BAD_REQUEST);
        }

        Authority authority = new Authority();
        authority.setFaculty(faculty);
        authority.setProfessor(professor);
        authority.setJobTitle(jobTitle);

        authority.setStartDate(authorityDTO.getStartDate());
        authority.setEndDate(authorityDTO.getEndDate());
        authority.setCreatedDate(authorityDTO.getCreatedDate());
        authority.setModifiedDate(authorityDTO.getModifiedDate());
        authority.setActive(authorityDTO.getActive());

        this.authorityRepository.save(authority);
        return authorityDTO;
    }

    @Override
    public AuthorityDTO findByUuid(String uuid) {
        return null;
    }

    @Override
    public List<AuthorityDTO> findAll() {
        return null;
    }

    @Override
    public List<AuthorityDTO> findAllActive() {
        return null;
    }

    @Override
    public AuthorityDTO logicalDelete(String uuid) {
        return null;
    }
}

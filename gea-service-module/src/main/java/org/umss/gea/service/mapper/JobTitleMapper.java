package org.umss.gea.service.mapper;

import org.springframework.stereotype.Component;
import org.umss.gea.domain.JobTitle;
import org.umss.gea.dto.JobTitleDTO;
import org.umss.gea.service.CustomMapper;

@Component
public class JobTitleMapper implements CustomMapper<JobTitleDTO, JobTitle> {
    @Override
    public JobTitleDTO toDto(JobTitle jobTitle) {
        JobTitleDTO jobTitleDTO = new JobTitleDTO();
        jobTitleDTO.setUuid(jobTitle.getUuid());
        jobTitleDTO.setNameTitle(jobTitle.getNameTitle());
        jobTitleDTO.setCreatedDate(jobTitle.getCreatedDate());
        jobTitleDTO.setModifiedDate(jobTitle.getModifiedDate());
        jobTitleDTO.setActive(jobTitle.getActive());

        return jobTitleDTO;
    }

    @Override
    public JobTitle toEntity(JobTitleDTO jobTitleDTO) {
        JobTitle jobTitle = new JobTitle();
        jobTitle.setUuid(jobTitleDTO.getUuid());
        jobTitle.setNameTitle(jobTitleDTO.getNameTitle());
        jobTitle.setCreatedDate(jobTitleDTO.getCreatedDate());
        jobTitle.setModifiedDate(jobTitleDTO.getModifiedDate());
        jobTitle.setActive(jobTitleDTO.getActive());

        return jobTitle;
    }
}

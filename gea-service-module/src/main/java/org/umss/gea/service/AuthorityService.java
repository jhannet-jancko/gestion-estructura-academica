package org.umss.gea.service;

import org.umss.gea.dto.AuthorityDTO;

public interface AuthorityService extends CrudServiceBase<AuthorityDTO, String>{
}

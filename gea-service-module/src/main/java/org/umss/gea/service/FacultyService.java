package org.umss.gea.service;

import org.umss.gea.dto.AuthorityDTO;
import org.umss.gea.dto.FacultyDTO;

import java.util.List;

public interface FacultyService extends CrudServiceBase<FacultyDTO, String> {
    FacultyDTO updateFaculty(FacultyDTO facultyDTO);
    List<AuthorityDTO> getAuthorities(String facultyUuid);
}

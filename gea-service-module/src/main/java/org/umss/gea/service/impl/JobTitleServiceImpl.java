package org.umss.gea.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.umss.gea.dto.JobTitleDTO;
import org.umss.gea.repository.JobTitleRepository;
import org.umss.gea.service.JobTitleService;
import org.umss.gea.service.mapper.JobTitleMapper;

import java.util.ArrayList;
import java.util.List;

@Service
public class JobTitleServiceImpl implements JobTitleService {

    private  final JobTitleRepository jobTitleRepository;
    private final JobTitleMapper jobTitleMapper;

    public JobTitleServiceImpl(JobTitleRepository jobTitleRepository, JobTitleMapper jobTitleMapper) {
        this.jobTitleRepository = jobTitleRepository;
        this.jobTitleMapper = jobTitleMapper;
    }

    @Override
    public JobTitleDTO save(JobTitleDTO jobTitleDTO) {
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public JobTitleDTO findByUuid(String uuid) {
        return jobTitleMapper.toDto(jobTitleRepository.findOneByUuid(uuid));
    }

    @Override
    @Transactional(readOnly = true)
    public List<JobTitleDTO> findAll() {
        List<JobTitleDTO> jobTitles = new ArrayList<>();
        jobTitleRepository.findAll().forEach(jobTitle-> jobTitles.add(jobTitleMapper.toDto(jobTitle)));
        return jobTitles;
    }

    @Override
    @Transactional(readOnly = true)
    public List<JobTitleDTO> findAllActive() {
        List<JobTitleDTO> jobTitles = new ArrayList<>();
        jobTitleRepository.findAll().forEach(jobTitle-> jobTitles.add(jobTitleMapper.toDto(jobTitle)));
        List<JobTitleDTO> activeJobTitles = jobTitles.stream().filter(JobTitleDTO::getActive).toList();
        return activeJobTitles;
    }

    @Override
    public JobTitleDTO logicalDelete(String uuid) {
        return null;
    }
}

package org.umss.gea.service;

import java.util.List;

public interface CrudServiceBase<DTO, T> {
    DTO save(DTO dto);
    DTO findByUuid(T uuid);
    List<DTO> findAll();
    List<DTO> findAllActive();
    DTO logicalDelete(T uuid);
}

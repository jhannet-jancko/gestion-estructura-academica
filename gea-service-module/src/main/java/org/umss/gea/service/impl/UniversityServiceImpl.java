package org.umss.gea.service.impl;

import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.umss.gea.common.exceptions.ResourceNotFoundException;
import org.umss.gea.domain.Faculty;
import org.umss.gea.domain.University;
import org.umss.gea.dto.FacultyDTO;
import org.umss.gea.dto.UniversityDTO;
import org.umss.gea.repository.FacultyRepository;
import org.umss.gea.repository.UniversityRepository;
import org.umss.gea.service.UniversityService;
import org.umss.gea.service.mapper.FacultyMapper;
import org.umss.gea.service.mapper.UniversityMapper;

import java.util.List;
import java.util.Optional;

import static org.umss.gea.domain.utils.constants.DBConstants.UNIVERSITY_TABLE_NAME_DB;

@Service
@Transactional
public class UniversityServiceImpl implements UniversityService {

    private final UniversityRepository universityRepository;
    private final UniversityMapper universityMapper;
    private final FacultyRepository facultyRepository;
    private final FacultyMapper facultyMapper;

    public UniversityServiceImpl(
            UniversityRepository universityRepository,
            UniversityMapper universityMapper,
            FacultyRepository facultyRepository,
            FacultyMapper facultyMapper) {
        this.universityRepository = universityRepository;
        this.universityMapper = universityMapper;
        this.facultyRepository = facultyRepository;
        this.facultyMapper = facultyMapper;
    }

    @Override
    public UniversityDTO save(UniversityDTO universityDTO) {
        University university = universityMapper.toEntity(universityDTO);
        return universityMapper.toDto(universityRepository.save(university));
    }

    @Override
    @Transactional(readOnly = true)
    public UniversityDTO findByUuid(String uuid) {
        University universityExample = new University(uuid);
        Optional<University> optionalUniversity = universityRepository.findOne(Example.of(universityExample));
        if (optionalUniversity.isEmpty()) {
            throw new ResourceNotFoundException(
                    String.format("University with uuid: %s does not exist", uuid),
                    UNIVERSITY_TABLE_NAME_DB);
        }
        return universityMapper.toDto(optionalUniversity.get());
    }

    @Override
    @Transactional(readOnly = true)
    public List<UniversityDTO> findAll() {
        return universityRepository.findAll().stream().map(universityMapper::toDto).toList();
    }

    @Override
    @Transactional(readOnly = true)
    public List<UniversityDTO> findAllActive() {
        List<University> activeUniversities = universityRepository.findAll().stream()
                .filter(University::getActive)
                .toList();
        return activeUniversities.stream().map(universityMapper::toDto).toList();
    }

    @Override
    public UniversityDTO logicalDelete(String uuid) {
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public List<FacultyDTO> getFaculties(String universityUuid) {
        University universityExample = new University();
        universityExample.setUuid(universityUuid);
        Optional<University> optionalUniversity = universityRepository.findOne(Example.of(universityExample));

        if (optionalUniversity.isEmpty()) {
            throw new ResourceNotFoundException(
                    String.format("University with uuid: %s does not exist", universityUuid),
                    UNIVERSITY_TABLE_NAME_DB);
        }

        University university = optionalUniversity.get();

        Faculty facultyExample = new Faculty();
        University universityFilterExample = new University();
        universityFilterExample.setId(university.getId());
        facultyExample.setUniversity(universityFilterExample);
        facultyExample.setActive(true);

        List<Faculty> faculties = facultyRepository.findAll(Example.of(facultyExample));
        return faculties
                .stream()
                .map(faculty -> facultyMapper.toDto(faculty, false))
                .toList();
    }
}

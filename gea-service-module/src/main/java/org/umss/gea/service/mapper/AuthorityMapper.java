package org.umss.gea.service.mapper;

import org.springframework.stereotype.Component;
import org.umss.gea.domain.Authority;
import org.umss.gea.domain.Faculty;
import org.umss.gea.domain.JobTitle;
import org.umss.gea.domain.Professor;
import org.umss.gea.dto.AuthorityDTO;

@Component
public class AuthorityMapper {
    public AuthorityDTO toDto(Authority authority) {
        AuthorityDTO authorityDTO = new AuthorityDTO();
        authorityDTO.setUuid(authority.getUuid());
        authorityDTO.setFacultyUuid(authority.getFaculty().getUuid());
        authorityDTO.setProfessorUuid(authority.getProfessor().getUuid());
        authorityDTO.setJobTitleUuid(authority.getJobTitle().getUuid());
        authorityDTO.setStartDate(authority.getStartDate());
        authorityDTO.setEndDate(authority.getEndDate());
        authorityDTO.setCreatedDate(authority.getCreatedDate());
        authorityDTO.setModifiedDate(authority.getModifiedDate());
        authorityDTO.setActive(authority.getActive());

        return authorityDTO;
    }

    public Authority toEntity(AuthorityDTO authorityDTO, Faculty faculty, Professor professor, JobTitle jobTitle) {
        Authority authority = new Authority();
        authority.setUuid(authorityDTO.getUuid());
        if (faculty != null)
            authority.setFaculty(faculty);
        if (professor != null)
            authority.setProfessor(professor);
        if (jobTitle != null)
            authority.setJobTitle(jobTitle);
        authority.setStartDate(authorityDTO.getStartDate());
        authority.setEndDate(authorityDTO.getEndDate());
        authority.setCreatedDate(authorityDTO.getCreatedDate());
        authority.setModifiedDate(authorityDTO.getModifiedDate());
        authority.setActive(authorityDTO.getActive());

        return authority;
    }
}

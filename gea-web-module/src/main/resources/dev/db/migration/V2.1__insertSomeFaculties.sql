insert into faculty (code, name, university_id, uuid, id)
values ('FCYT',  'Facultad de ciencias y tecnologia', 1000, 'aff6e021-90d9-45fe-bc0c-a8692e091623', 2000);
insert into faculty (code,  name, university_id, uuid, id)
values ('FCE', 'Facultad de Ciencias Economicos', 1000, 'aff6e021-90d9-45fe-bc0c-a8692e091624', 2001);
insert into faculty (code, name, university_id, uuid, id)
values ('FCJ', 'Facultad de ciencias juridicas', 1001, 'aff6e021-90d9-45fe-bc0c-a8692e091625', 2002);
insert into faculty (code, name, university_id, uuid, id, active)
values ('FCJ', 'Facultad de ciencias juridicas', 1000, 'aff6e021-90d9-45fe-bc0c-a8692e091626', 2003, false);
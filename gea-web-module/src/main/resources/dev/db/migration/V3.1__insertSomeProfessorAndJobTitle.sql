INSERT INTO public.professor(active, code, first_name, last_name, professional_title, uuid, id)
VALUES (true, '202200', 'Juan Jose', 'Cortez', 'Licenciatura en matematicas', 'aff6e021-90d9-45fe-bc0c-a8692e091630', 3000);
INSERT INTO public.professor(active, code, first_name, last_name, professional_title, uuid, id)
VALUES (true, '202201', 'Maria', 'Bustamante', 'Licenciatura en matematicas', 'aff6e021-90d9-45fe-bc0c-a8692e091631', 3001);
INSERT INTO public.professor(active, code, first_name, last_name, professional_title, uuid, id)
VALUES (true, '202202', 'Jhannet', 'Jancko Camacho', 'Ingenieria informatica', 'aff6e021-90d9-45fe-bc0c-a8692e091632', 3002);

INSERT INTO public.job_title(id, active, name_title, uuid)
VALUES (4000, true, 'Decano', 'aff6e021-90d9-45fe-bc0c-a8692e091640');
INSERT INTO public.job_title(id, active, name_title, uuid)
VALUES (4001, true, 'Director Academico', 'aff6e021-90d9-45fe-bc0c-a8692e091641');
INSERT INTO public.job_title(id, active, name_title, uuid)
VALUES (4002, true, 'Jefe de departamento', 'aff6e021-90d9-45fe-bc0c-a8692e091642');
INSERT INTO public.job_title(id, active, name_title, uuid)
VALUES (4003, true, 'Director de carrera', 'aff6e021-90d9-45fe-bc0c-a8692e091643');

INSERT INTO public.faculty_professor_title(faculty_id, professor_id, title_id, active, start_date, end_date, uuid, id)
VALUES (2000, 3000, 4000, true, '2001-09-28 01:00:00', '2002-09-28 01:00:00', 'aff6e021-90d9-45fe-bc0c-a8692e091650', 5000);
INSERT INTO public.faculty_professor_title(faculty_id, professor_id, title_id, active, start_date, end_date, uuid, id)
VALUES (2000, 3001, 4001, true, '2001-09-28 01:00:00', '2002-09-28 01:00:00', 'aff6e021-90d9-45fe-bc0c-a8692e091651', 5001);
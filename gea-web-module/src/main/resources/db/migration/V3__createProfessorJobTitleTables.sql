CREATE TABLE IF NOT EXISTS public.professor
(
    id SERIAL NOT NULL,
    active boolean NOT NULL DEFAULT true,
    code character varying(20) COLLATE pg_catalog."default" NOT NULL,
    created_date timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    first_name character varying(100) COLLATE pg_catalog."default" NOT NULL,
    last_name character varying(100) COLLATE pg_catalog."default" NOT NULL,
    modified_date timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    professional_title character varying(200) COLLATE pg_catalog."default" NOT NULL,
    uuid character varying(36) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT professor_pkey PRIMARY KEY (id),
    CONSTRAINT uk_kmpf2qwbhebru5d4jt1sx51g7 UNIQUE (uuid)
)

TABLESPACE pg_default;

CREATE TABLE IF NOT EXISTS public.job_title
(
    id SMALLSERIAL NOT NULL,
    active boolean NOT NULL DEFAULT true,
    created_date timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modified_date timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    name_title character varying(300) COLLATE pg_catalog."default" NOT NULL,
    uuid character varying(36) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT job_title_pkey PRIMARY KEY (id),
    CONSTRAINT uk_qorsvi4a3hlaws6lep5l6taf9 UNIQUE (uuid)
)

TABLESPACE pg_default;

create table faculty_professor_title (
   id SERIAL NOT NULL,
   faculty_id integer not null,
   professor_id integer not null,
   title_id integer not null,
   active boolean NOT NULL DEFAULT true,
   start_date timestamp without time zone,
   end_date timestamp without time zone,
   created_date timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
   modified_date timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
   uuid character varying(36) COLLATE pg_catalog."default" NOT NULL,
   primary key (faculty_id, professor_id, title_id)
);

alter table faculty_professor_title add constraint FK_faculty_professor_title_RefFaculty foreign key (faculty_id)
    references faculty (id) on delete restrict on update restrict;

alter table faculty_professor_title add constraint FK_faculty_professor_title_RefProfessor foreign key (professor_id)
    references professor (id) on delete restrict on update restrict;

alter table faculty_professor_title add constraint FK_faculty_professor_title_RefTitle foreign key (title_id)
    references job_title (id) on delete restrict on update restrict;
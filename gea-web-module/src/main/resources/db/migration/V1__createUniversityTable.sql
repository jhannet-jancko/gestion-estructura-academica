CREATE TABLE IF NOT EXISTS public.university
(
    id integer NOT NULL,
    code character varying(20) COLLATE pg_catalog."default" NOT NULL,
    created_date timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    active boolean NOT NULL DEFAULT true,
    modified_date timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    name character varying(500) COLLATE pg_catalog."default" NOT NULL,
    uuid character varying(36) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT university_pkey PRIMARY KEY (id),
    CONSTRAINT uk_5cfqaayg6w8q4jeowet707864 UNIQUE (uuid)
)
TABLESPACE pg_default;
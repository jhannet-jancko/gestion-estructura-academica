CREATE TABLE IF NOT EXISTS public.faculty
(
    id SERIAL NOT NULL,
    address character varying(500) COLLATE pg_catalog."default",
    code character varying(20) COLLATE pg_catalog."default" NOT NULL,
    created_date timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    active boolean NOT NULL DEFAULT true,
    email character varying(350) COLLATE pg_catalog."default",
    management_type character varying(255) COLLATE pg_catalog."default",
    mission character varying(1000) COLLATE pg_catalog."default",
    modified_date timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    name character varying(500) COLLATE pg_catalog."default" NOT NULL,
    telephone character varying(20) COLLATE pg_catalog."default",
    uuid character varying(36) COLLATE pg_catalog."default" NOT NULL,
    vision character varying(1000) COLLATE pg_catalog."default",
    web_site character varying(1000) COLLATE pg_catalog."default",
    university_id integer,
    CONSTRAINT faculty_pkey PRIMARY KEY (id),
    CONSTRAINT uk_s2cjffmqpnkgr4lrbm3avvwm9 UNIQUE (uuid),
    CONSTRAINT fkivqbiytd9en6sk09duabc6scc FOREIGN KEY (university_id)
    REFERENCES public.university (id) MATCH SIMPLE
                           ON UPDATE NO ACTION
                           ON DELETE NO ACTION
)

TABLESPACE pg_default;
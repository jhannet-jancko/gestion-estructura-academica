package org.umss.gea.configuration;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaRepositories({"org.umss.gea.repository"})
@EntityScan("org.umss.gea.domain")
@EnableTransactionManagement
public class DatabaseConfiguration {
}

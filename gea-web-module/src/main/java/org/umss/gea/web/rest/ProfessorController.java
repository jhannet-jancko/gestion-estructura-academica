package org.umss.gea.web.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.umss.gea.dto.ProfessorDTO;
import org.umss.gea.service.ProfessorService;

import java.util.List;

@RestController
@RequestMapping("/v1/professors")
public class ProfessorController {

    private final ProfessorService professorService;

    public ProfessorController(ProfessorService professorService) {
        this.professorService = professorService;
    }

    @GetMapping
    public ResponseEntity<List<ProfessorDTO>> getAll(@RequestParam(name = "justActive", defaultValue = "true") boolean justActive) {
        if (justActive) {
            return ResponseEntity.ok().body(professorService.findAllActive());
        } else {
            return ResponseEntity.ok().body(professorService.findAll());
        }
    }

    @GetMapping("/{uuid}")
    public ResponseEntity<ProfessorDTO> get(@PathVariable String uuid) {
        ProfessorDTO professorDTO = professorService.findByUuid(uuid);
        return ResponseEntity.ok().body(professorDTO);
    }
}

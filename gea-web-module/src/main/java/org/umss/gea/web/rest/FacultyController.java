package org.umss.gea.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.umss.gea.common.exceptions.BadRequestAlertException;
import org.umss.gea.dto.AuthorityDTO;
import org.umss.gea.dto.FacultyDTO;
import org.umss.gea.service.FacultyService;

import java.net.URI;
import java.util.List;
import java.util.Objects;

import static org.umss.gea.common.exceptions.ErrorConstants.ID_INVALID_BAD_REQUEST;
import static org.umss.gea.common.exceptions.ErrorConstants.ID_NULL_BAD_REQUEST;
import static org.umss.gea.domain.utils.constants.DBConstants.FACULTY_TABLE_NAME_DB;
import static org.umss.gea.domain.utils.constants.DBConstants.UNIVERSITY_TABLE_NAME_DB;

@RestController
@RequestMapping("/v1/faculties")
public class FacultyController {

    private final Logger log = LoggerFactory.getLogger(FacultyController.class);
    private final FacultyService facultyService;

    public FacultyController(FacultyService facultyService) {
        this.facultyService = facultyService;
    }

    @GetMapping
    public ResponseEntity<List<FacultyDTO>> getAll(@RequestParam(name = "justActive", defaultValue = "true") boolean justActive) {
        if (justActive) {
            return ResponseEntity.ok().body(facultyService.findAllActive());
        } else {
            return ResponseEntity.ok().body(facultyService.findAll());
        }
    }

    @GetMapping("/{uuid}")
    public ResponseEntity<FacultyDTO> get(@PathVariable String uuid) {
        FacultyDTO facultyDTO = facultyService.findByUuid(uuid);
        return ResponseEntity.ok().body(facultyDTO);
    }

    @PostMapping
    public ResponseEntity<FacultyDTO> create(@RequestBody FacultyDTO facultyDTO) {
        log.debug("REST request to create a Faculty : {}", facultyDTO);
        FacultyDTO createdFacultyDTO = facultyService.save(facultyDTO);

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{facultyUuid}")
                .buildAndExpand(createdFacultyDTO.getUuid())
                .toUri();

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .header(HttpHeaders.LOCATION, uri.toString())
                .body(createdFacultyDTO);
    }

    @PutMapping("/{uuid}")
    public ResponseEntity<FacultyDTO> update(@PathVariable String uuid, @RequestBody FacultyDTO facultyDTO) {
        log.debug("REST request to update Faculty : {}, {}", uuid, facultyDTO);

        if (facultyDTO.getUuid() == null) {
            throw new BadRequestAlertException("Invalid Faculty UUID, null value", FACULTY_TABLE_NAME_DB, ID_NULL_BAD_REQUEST);
        }

        if (!Objects.equals(uuid, facultyDTO.getUuid())) {
            throw new BadRequestAlertException("Invalid Faculty UUID,  differents ids", FACULTY_TABLE_NAME_DB, ID_INVALID_BAD_REQUEST);
        }

        FacultyDTO updatedFacultyDTO = facultyService.updateFaculty(facultyDTO);

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{facultyUuid}")
                .buildAndExpand(updatedFacultyDTO.getUuid())
                .toUri();

        return ResponseEntity
                .status(HttpStatus.OK)
                .header(HttpHeaders.LOCATION, uri.toString())
                .body(updatedFacultyDTO);
    }

    @DeleteMapping("/{uuid}")
    public ResponseEntity<FacultyDTO> delete(@PathVariable String uuid) {
        return ResponseEntity.ok().body(facultyService.logicalDelete(uuid));
    }

    @GetMapping("/{facultyUuid}/authorities")
    public ResponseEntity<List<AuthorityDTO>> getAuthorities(@PathVariable String facultyUuid) {
        if (facultyUuid == null) {
            throw new BadRequestAlertException(
                    "Invalid Authority uuid, null value",
                    FACULTY_TABLE_NAME_DB,
                    ID_NULL_BAD_REQUEST);
        }
        return ResponseEntity.ok().body(facultyService.getAuthorities(facultyUuid));
    }
}

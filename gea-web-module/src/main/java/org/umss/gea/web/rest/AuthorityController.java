package org.umss.gea.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.umss.gea.dto.AuthorityDTO;
import org.umss.gea.service.AuthorityService;

import java.net.URI;

@RestController
@RequestMapping("/v1/authorities")
public class AuthorityController {
    private final Logger log = LoggerFactory.getLogger(AuthorityController.class);
    private final AuthorityService authorityService;

    public AuthorityController(AuthorityService authorityService) {
        this.authorityService = authorityService;
    }

    @PostMapping
    public ResponseEntity<AuthorityDTO> create(@RequestBody AuthorityDTO authorityDTO) {
        log.debug("REST request to register an authority : {}", authorityDTO);
        AuthorityDTO createdAuthorityDTO = authorityService.save(authorityDTO);
        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{facultyUuid}")
                .buildAndExpand(createdAuthorityDTO.getUuid())
                .toUri();

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .header(HttpHeaders.LOCATION, uri.toString())
                .body(createdAuthorityDTO);
    }
}

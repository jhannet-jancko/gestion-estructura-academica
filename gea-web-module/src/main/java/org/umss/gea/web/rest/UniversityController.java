package org.umss.gea.web.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.umss.gea.dto.FacultyDTO;
import org.umss.gea.dto.UniversityDTO;
import org.umss.gea.service.UniversityService;
import org.umss.gea.common.exceptions.BadRequestAlertException;

import java.util.List;

import static org.umss.gea.domain.utils.constants.DBConstants.UNIVERSITY_TABLE_NAME_DB;
import static org.umss.gea.common.exceptions.ErrorConstants.ID_NULL_BAD_REQUEST;

@RestController
@RequestMapping("/v1/universities")
public class UniversityController {

    private final UniversityService universityService;

    public UniversityController(UniversityService universityService) {
        this.universityService = universityService;
    }

    @GetMapping
    public ResponseEntity<List<UniversityDTO>> getAll(@RequestParam(name = "justActive", defaultValue = "true") boolean justActive) {
        if (justActive) {
            return ResponseEntity.ok().body(universityService.findAllActive());
        } else {
            return ResponseEntity.ok().body(universityService.findAll());
        }
    }

    @GetMapping("/{uuid}")
    public ResponseEntity<UniversityDTO> get(@PathVariable String uuid) {
        UniversityDTO universityDTO = universityService.findByUuid(uuid);
        return ResponseEntity.ok().body(universityDTO);
    }

    @GetMapping("/{universityUuid}/faculties")
    public ResponseEntity<List<FacultyDTO>> getFaculties(@PathVariable String universityUuid) {
        if (universityUuid == null) {
            throw new BadRequestAlertException(
                    "Invalid University uuid, null value",
                    UNIVERSITY_TABLE_NAME_DB,
                    ID_NULL_BAD_REQUEST);
        }
        return ResponseEntity.ok().body(universityService.getFaculties(universityUuid));
    }
}

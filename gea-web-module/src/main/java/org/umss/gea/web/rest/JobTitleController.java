package org.umss.gea.web.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.umss.gea.dto.JobTitleDTO;
import org.umss.gea.service.JobTitleService;

import java.util.List;

@RestController
@RequestMapping("/v1/titles")
public class JobTitleController {
    private final JobTitleService jobTitleService;

    public JobTitleController(JobTitleService jobTitleService) {
        this.jobTitleService = jobTitleService;
    }

    @GetMapping
    public ResponseEntity<List<JobTitleDTO>> getAll(@RequestParam(name = "justActive", defaultValue = "true") boolean justActive) {
        if (justActive) {
            return ResponseEntity.ok().body(jobTitleService.findAllActive());
        } else {
            return ResponseEntity.ok().body(jobTitleService.findAll());
        }
    }

    @GetMapping("/{uuid}")
    public ResponseEntity<JobTitleDTO> get(@PathVariable String uuid) {
        JobTitleDTO jobTitleDTO = jobTitleService.findByUuid(uuid);
        return ResponseEntity.ok().body(jobTitleDTO);
    }
}
